"use strict";

const html = (tag, props = {}) => (...children) => {
  const element = document.createElement(tag);
  Object.keys(props).forEach(k => {
    const prop = props[k];
    if (k === "style") {
      Object.keys(props[k]).forEach(styleKey => {
        element.style.setProperty(styleKey, prop[styleKey]);
      });
    } else if (k === "className") {
      element.className = prop;
    } else {
      element.setAttribute(k, prop);
    }
  });
  children.forEach(item => {
    const node = (typeof item === "string") ? document.createTextNode(item) : item;
    element.appendChild(node);
  });
  return element;
}

const text = value => document.createTextNode(value);

const render = (element, child) => {
  while (element.firstChild) {
    element.firstChild.remove();
  }
  element.appendChild(child);
}

/* View Functions */
const linkRegex = /https?:\/\/\S+/gi;

const viewTextMessage = (m, index) => {
  const links = m.content.body.match(linkRegex);
  const link = links ? links[0] : null;
  const viewLinks =
    link
      ? html("a", {
        href: link,
        target: "_blank",
        "data-messagelink": true,
      })(link)
      : text("");

  const tags = [];
  if (link) {
    tags.push("url");
  }
  if ((/github\.io|github\.com|gitlab\.com/).test(m.content.body)) {
    tags.push("repo");
  }
  if ((/youtube\.com|youtu\.be/).test(m.content.body)) {
    tags.push("video");
  }
  if ((/reddit\.com|redd\.it/).test(m.content.body)) {
    tags.push("reddit");
  }
  if ((/wikipedia/).test(m.content.body)) {
    tags.push("wikipedia");
  }
  if ((/blog/).test(m.content.body)) {
    tags.push("blog");
  }
  if ((/arxiv/).test(m.content.body)) {
    tags.push("arxiv");
  }

  if ((/news\.ycombinator/).test(m.content.body)) {
    tags.push("hacker_news");
  }

  return html("div")(
    link ? `🌐 Link message` : "",
    html("pre")(html("code")(m.content.body)),
    viewLinks,
    html("p")(
      html("label", { for: `folder-${index}` })("Folder"),
      html("br")(),
      html("input", {
        type: "text",
        id: `folder-${index}`,
        name: "folder",
        list: "vault-folders",
        value: "weblinks",
      })(),
    ),
    html("p")(
      html("label", { for: `page_name-${index}` })("Page Name"),
      html("br")(),
      html("input", {
        type: "text",
        id: `page_name-${index}`,
        name: "page_name",
        required: true
      })(),
    ),
    html("p")(
      html("label", { for: `page_tags-${index}` })("Tags"),
      html("br")(),
      html("input", {
        type: "text",
        id: `page_tags-${index}`,
        name: "page_tags",
        required: true,
        value: tags.join(", "),
      })(),
    ),
    html("p")(
      html("label", { for: `page_frontmatter-${index}` })("More frontmatter"),
      html("br")(),
      html("textarea", {
        id: `page_frontmatter-${index}`,
        name: "page_frontmatter",
        required: true,
      })(link ? `url: ${link}` : ""),
    ),
    html("p")(
      html("label", { for: `page_content-${index}` })("Content"),
      html("br")(),
      html("textarea", {
        id: `page_content-${index}`,
        name: "page_content",
        required: true,
      })(`# {{title}}
${link ? "\n" + link : ""}

> {{description}}`),
    ),
  )
};

const viewFile = (m, index) => {
  const fileURL =
    `https://matrix-client.matrix.org/_matrix/media/r0/download/${m.content.url.replace("mxc://", "")}`;

  return html("div")(
    html("span")(`💾 ${m.content?.info?.mimetype ? m.content.info.mimetype : "file"} `),
    html("br")(),
    html("a", { href: fileURL, target: "_blank" })(m.content.body),
    html("p")(
      html("label", { for: `folder-${index}` })("Folder"),
      html("br")(),
      html("input", {
        type: "text",
        id: `folder-${index}`,
        name: "folder",
        list: "vault-folders",
        value: "files",
      })(),
    ),
    html("p")(
      html("label", { for: `page_name-${index}` })("Page Name"),
      html("br")(),
      html("input", {
        type: "text",
        id: `page_name-${index}`,
        name: "page_name",
        value: m.content.body,
        required: true
      })(),
    ),
    html("p")(
      html("label", { for: `page_tags-${index}` })("Tags"),
      html("br")(),
      html("input", {
        type: "text",
        id: `page_tags-${index}`,
        name: "page_tags",
        required: true,
      })(),
    ),
    html("p")(
      html("label", { for: `page_frontmatter-${index}` })("More frontmatter"),
      html("br")(),
      html("textarea", {
        id: `page_frontmatter-${index}`,
        name: "page_frontmatter",
        required: true,
      })(""),
    ),
    html("p")(
      html("label", { for: `page_content-${index}` })("Content"),
      html("br")(),
      html("textarea", {
        id: `page_content-${index}`,
        name: "page_content",
        required: true,
      })(),
    ),
  )
};

const viewImage = (m, index) => {

  const imageURL =
    `https://matrix-client.matrix.org/_matrix/media/r0/download/${m.content.url.replace("mxc://", "")}`;

  return html("div")(
    `🖼 Image:`,
    html("a", { href: imageURL, target: "_blank" })(
      html("figure")(
        html("img", {
          src: imageURL,
          alt: m.content.body,
        })(),
        html("figcaption")(m.content.body),
      )),

    html("p")(
      html("label", { for: `folder-${index}` })("Folder"),
      html("br")(),
      html("input", {
        type: "text",
        id: `folder-${index}`,
        name: "folder",
        list: "vault-folders",
        value: "files",
      })(),
    ),
    html("p")(
      html("label", { for: `page_name-${index}` })("Page Name"),
      html("br")(),
      html("input", {
        type: "text",
        id: `page_name-${index}`,
        name: "page_name",
        value: m.content.body,
        required: true
      })(),
    ),
    html("p")(
      html("label", { for: `page_tags-${index}` })("Tags"),
      html("br")(),
      html("input", {
        type: "text",
        id: `page_tags-${index}`,
        name: "page_tags",
        required: true,
      })(),
    ),
    html("p")(
      html("label", { for: `page_frontmatter-${index}` })("More frontmatter"),
      html("br")(),
      html("textarea", {
        id: `page_frontmatter-${index}`,
        name: "page_frontmatter",
        required: true,
      })(`foo`),
    ),
    html("p")(
      html("label", { for: `page_content-${index}` })("Content"),
      html("br")(),
      html("textarea", {
        id: `page_content-${index}`,
        name: "page_content",
        required: true,
      })(),
    ),
  )
};

const contentType = {
  "m.text": viewTextMessage,
  "m.file": viewFile,
  "m.image": viewImage,
};

const viewMatrixEvent = (m, index) => html("form", { "data-eventid": m.event_id, "data-index": index })(
  html("br")(),
  html("hr")(),
  html("input", { type: "hidden", name: "event_id", value: m.event_id })(),
  (
    contentType[m.content.msgtype]
      ? contentType[m.content.msgtype](m, index)
      : `Unknown msgtype: ${m.content.msgtype}`
  ),
  html("p", { "data-controls": true })(
    html("button", { type: "submit" })(`+ Add to Obsidian`),
    html("button", { type: "button", className: "secondary" })("Hide"),
  ),
)


/* Elements */
const fetchEventsForm = document.getElementById("fetch-events");
const scrapeHyperlinksEl = document.getElementById("scrape-hyperlinks");
const resultsEl = document.getElementById("results");
const cursorStartEl = document.getElementById("cursor_start");
const fetchCursorsForm = document.getElementById("fetch-cursors");


/* API */

const getInitialCursorStart = async () => {
  const response = await fetch("/cursor", {
    method: 'GET',
    cache: 'no-cache',
  });

  const responseData = await response.json();
  cursorStartEl.value = responseData.CURSOR_START;
};

getInitialCursorStart();

fetchCursorsForm.addEventListener("submit", (e) => {
  e.preventDefault();
  cursorStartEl.value = fetchCursorsForm.getAttribute("data-end");
});

fetchEventsForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  const data = Object.fromEntries((new FormData(fetchEventsForm)).entries());

  const response = await fetch("/events", {
    method: 'POST',
    cache: 'no-cache',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  });

  const responseData = await response.json();

  // fetch-cursors
  fetchCursorsForm.setAttribute("data-end", responseData.end);
  render(fetchCursorsForm,
    html("div")(
      html("pre")(html("code")(
        `Start: ${responseData.start}`,
        `\n`,
        `  End: ${responseData.end}`,
      )),
      html("button", { type: "submit" })("Update Cursor"),
    )
  );

  render(resultsEl,
    html("div")(
      html("datalist", { id: "vault-folders" })(
        ...responseData.vaultFolders.map(value => html("option", { value })())
      ),
      html("div")(
        ...responseData.chunk.map(viewMatrixEvent)
      )
    )
  );

  if (!scrapeHyperlinksEl.checked) {
    return;
  }
  setTimeout(async () => {
    const links = document.querySelectorAll("a[data-messagelink]");

    let linksRemaining = links.length;

    console.log(`links to load : ${linksRemaining}`);

    await Promise.all([...links].map(async (linkEl) => {

      linkEl.setAttribute("data-loading", true);

      const response = await fetch("/proxy", {
        method: 'POST',
        cache: 'no-cache',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ url: linkEl.href }),
      });
      const { title, description, keywords } = await response.json();

      const form = linkEl.closest("form");
      const pageContentEl = form.querySelector(`textarea[name="page_content"]`);
      if (title) {
        form
          .querySelector(`input[name="page_name"]`)
          .value =
          title
            .replace(/[^\w\d-()\s]/gi, "")
            .trim()
            .replace(/\s+/g, " ");
        pageContentEl.value = pageContentEl.value.replace("{{title}}", title);
      }
      if (description) {
        pageContentEl.value = pageContentEl.value.replace("{{description}}", description);
      }

      if (keywords.length) {
        const tagsEl = form.querySelector(`input[name="page_tags"]`)
        const extraTags = keywords.map(v => v.replace(/\s/g, "_")).join(", ");
        tagsEl.value = tagsEl.value + ", " + extraTags;
      }

      linkEl.setAttribute("data-loading", false);
      linksRemaining = linksRemaining - 1;
      if (linksRemaining !== 0) {
        console.log(`links to load : ${linksRemaining}`);
      } else {
        console.log("all links loaded");
      }

      return responseData;
    }));
  }, 1);

});

/* Hide event */
resultsEl.addEventListener("click", e => {
  const button = e.target;
  if (button.innerText === "Hide") {
    const form = button.closest("form");
    form.style.setProperty('opacity', "0.1");
    setTimeout(() => {
      form.style.setProperty("display", "none");
    }, 500);
  }
});

/* Add to Obsidian */
resultsEl.addEventListener("submit", async e => {
  e.preventDefault();
  const form = e.target;
  form.querySelectorAll(`p[data-controls] button`).forEach(el => { el.disabled = true });

  const data = Object.fromEntries((new FormData(form)).entries());

  const response = await fetch("/page", {
    method: 'POST',
    cache: 'no-cache',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  });

  const responseData = await response.json();
  console.log(responseData);

  form.style.setProperty('opacity', "0.1");
  setTimeout(() => {
    form.remove();
  }, 500);
});

