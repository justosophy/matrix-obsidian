const http = require('http');
const https = require('https');
const fs = require('fs');

const playwright = require('playwright');
const KEYWORDS = require('./keywords');

const main = async () => {

    // Populates process.env from .env file
    const config = {};

    const parseConfig = configObject => {
        const file = fs.readFileSync(`${__dirname}/.env`, { encoding: 'utf8' });
        file.split("\n").forEach(line => {
            if (line.length === 0) {
                return;
            }
            const [key, value] = line.split("=");
            configObject[key] = value;
        });
    }
    parseConfig(config);

    const hostname = '127.0.0.1';
    const port = 3000;

    const vaultFolders =
        fs.readdirSync(config.VAULT + "/content", "utf8")
            .filter(value => !(/\./.test(value)));

    /* files */
    const index = (req, res) => {
        fs.readFile(`${__dirname}/public/index.html`, function (err, data) {
            res.setHeader('Content-type', 'text/html');
            res.end(data);
        });
    }

    const css = (req, res) => {
        fs.readFile(`${__dirname}/public/simple.css`, function (err, data) {
            res.setHeader('Content-type', 'text/css');
            res.end(data);
        });
    }

    const js = (req, res) => {
        fs.readFile(`${__dirname}/public/app.js`, function (err, data) {
            res.setHeader('Content-type', 'text/javascript');
            res.end(data);
        });
    }

    /* data */
    const parseMatrixEvents = async (data) => {
        data.chunk = data.chunk.filter(m => m.type === "m.room.message" && m.content?.body);
        data.vaultFolders = vaultFolders;
        data.keywords = KEYWORDS;
        return data;
    }

    /* api */

    const fetchCursor = async (req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');

        const response = {
            CURSOR_START: config.CURSOR_START
        }

        res.end(JSON.stringify(response));
    }

    const fetchMatrixEvents = async (req, res) => {
        const buffers = [];
        for await (const chunk of req) {
            buffers.push(chunk);
        }
        const dataString = Buffer.concat(buffers).toString();
        const data = JSON.parse(dataString)

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');

        console.log(data);

        const matrixReq = https.request({
            hostname: config.DELEGATED_URL,
            port: 443,
            path: `/_matrix/client/r0/rooms/${config.ROOM_ID}/messages?from=${data.cursor_start}&dir=${data.direction}&limit=${config.EVENT_LIMIT}`,
            method: 'GET',
            headers: {
                Authorization: `Bearer ${config.USER_TOKEN}`,
            },
        }, async matrixRes => {
            const buffers = [];
            for await (const chunk of matrixRes) {
                buffers.push(chunk);
            }
            const matrixDataString = Buffer.concat(buffers).toString();
            const data = await parseMatrixEvents(JSON.parse(matrixDataString));
            res.end(JSON.stringify(data));
        });
        matrixReq.on('error', error => {
            console.error(error);
        });
        matrixReq.end();

    }

    const browser = await playwright.chromium.launch({
        headless: true, // setting this to true will not run the UI
        bypassCSP: true,
        acceptDownloads: false,
    });

    const proxyRequest = async (req, res) => {

        const buffers = [];
        for await (const chunk of req) {
            buffers.push(chunk);
        }
        const dataString = Buffer.concat(buffers).toString();
        const data = JSON.parse(dataString);

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');

        const responseData = {
            url: data.url,
            title: '',
            description: '',
            keywords: [],
        }

        const page = await browser.newPage();
        const descriptionLocator = page.locator('meta[name="description"]');

        try {
            console.log(`proxy fetching ${data.url}`);
            await page.goto(data.url);
        } catch {
            page.close();
            res.end(JSON.stringify(responseData));
            return;
        }

        responseData.title = await page.title();
        await page.waitForTimeout(3000); // wait for 3 seconds

        try {
            responseData.description = await descriptionLocator.evaluate(node => node?.getAttribute("content"), { timeout: 1000 });
        } catch { }

        const bodyText = await page.innerText("body");

        const keyWords = KEYWORDS.filter(term => {
            const escapedTerm = term.replace(/(\W)/gi, "\\$1");
            const regex = new RegExp("\\W" + escapedTerm + "\\W", "i");
            return regex.test(bodyText);
        })

        responseData.keywords = keyWords;

        keyWords.forEach(term => {
            const regex = new RegExp("(\\W)(" + term.replace(/(\W)/gi, "\\$1") + ")(\\W)", "gi");
            responseData.description = responseData.description.replace(regex, "$1[[$2]]$3");
        });

        page.close();
        res.end(JSON.stringify(responseData));
    }

    const submitPage = async (req, res) => {
        const buffers = [];
        for await (const chunk of req) {
            buffers.push(chunk);
        }
        const dataString = Buffer.concat(buffers).toString();
        const data = JSON.parse(dataString);

        console.log(`Submit page: "${data.page_name}"`);

        const folder = data.folder ? `${config.VAULT}/content/${data.folder}` : `${config.VAULT}/content`;

        if (!fs.existsSync(folder)){
            fs.mkdirSync(folder, { recursive: true });
        }

        let fileContent = '';
        if (data.page_tags || data.page_frontmatter) {
            const frontMatter = [`---`];
            if (data.page_tags) {
                frontMatter.push(`tags: ${data.page_tags}`);
            }
            if (data.page_frontmatter) {
                frontMatter.push(data.page_frontmatter);
            }
            frontMatter.push(`---`);
            fileContent = frontMatter.join("\n") + "\n\n";
        }
        fileContent = fileContent + data.page_content;

        console.log(`\n\n${fileContent}\n\n`);

        try {
            fs.writeFileSync(`${folder}/${data.page_name}.md`, fileContent, { encoding: 'utf8' });
        } catch (e) {
            console.log(e);
            res.statusCode = 500;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify({ error: e }));
            return;
        }

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(data));
    }

    const notFound = (req, res) => {
        res.statusCode = 404;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Not found');
    }



    const routes = {
        "/": index,
        "/simple.css": css,
        "/app.js": js,
        "/cursor": fetchCursor,
        "/events": fetchMatrixEvents,
        "/proxy": proxyRequest,
        "/page": submitPage,
    }

    const server = http.createServer((req, res) => {
        console.log(`Request: "${req.url}"`);
        const handler = routes[req.url];
        if (handler) {
            handler(req, res);
        } else {
            notFound(req, res);
        }
    });

    server.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/`);
    });
}

main();